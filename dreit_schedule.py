# This is a part of code I wrote for DynaSpAM project in Libert Research Group
# in Princeton University.
# This script post-processes text log files generated from GEM5 architecture
# simulation.
# DynaSpAM project has been published as:
# "Feng Liu, Heqjin Ahn, Stephen R. Beard, Taewook Oh, and David I. August,
# DynaSpAM: Dynamic Spatial Architecture Mapping using Out of Order Instruction
# Schedules, in Proceedings of the 42nd International Symposium on Computer
# Architecture (ISCA), June 2015."

# This script assumes the schedule print format that looks like below
#
# 478036500: reg_dep_unit: Producers of instruction sources:
# 478036500: reg_dep_unit: ldl        r1,-31008(r28)    inst (0,14)|    src phy(42) (livein)
# 478036500: reg_dep_unit: addl       r24,1,r24    inst (0,0)|    src phy(140) (livein)
# 478036500: reg_dep_unit: lda        r25,8(r25)    inst (0,1)|    src phy(159) (livein)
# 478036500: reg_dep_unit: bis        r31,r11,r2    inst (0,2)|    src phy(31) (livein)    src phy(29) (livein)
# ...
# 478036500: reg_dep_unit: PriorityRanking: 5 6 7 8 9 10 11 12 13 14 15 16 17 0 1 3 4 2

import os
import sys
import re
import copy
import gem5_to_mcpat

# Paths
gem5_path = os.getenv('GEM5_HOME')
rodinia_dir = 'benchmark_rodinia/alpha/'
rodinia_path = gem5_path + '/' + rodinia_dir
m5out_dir = 'm5out/'
trace_file = m5out_dir + 'trace.out'
conf_file = m5out_dir + 'config.ini'
stat_file = m5out_dir + 'stat.out'
base_stat_file = m5out_dir + 'base.stat.out'
mcpat_path = gem5_path + '/mcpat/mcpat'
mcpat_template_path = gem5_path + '/scripts/mcpat_template.xml'

rodinia_benchmarks = [
  'b+tree',
  'backprop',
  'bfs',
  'hotspot',
  'kmeans_serial',
  'lud',
  'nn',
  'nw',
  'particlefilter',
  'pathfinder',
  'srad_v2',
]

# Schedule prettyprint
OPCODE_PRINT_WIDTH = 7
INST_PRINT_WIDTH = 50

# Trace prefix
fab_prefix = 'reg_dep_unit'

# Regular expressions

# One line format examples
# ex) 563633000: reg_dep_unit: bis        r31,r12,r5    inst (0,0)|    src phy(31) (livein)    src phy(12) (livein)
line_rx = re.compile('\s*\d+: [\w.]+: (.*)')

# Schedule prolog
# ex) Producers of instruction sources: trace_idx = 10  entry_idx = 1000
sched_prolog_rx = re.compile('Producers of instruction sources:\s*trace_idx\s*=\s*(\d+)\s*entry_idx\s*=\s*(\d+)')

# Instruction string format in trace
# Format: OPCODE OPERANDS inst (ROW,COL)
# ex) bis        r31,r2,r12    inst (1,1)
# NOP has a slightly different format
# ex) nop        (ldq_u      r31,0(r30))    inst (-1,-1)|
inst_rx = re.compile('(\w+\s+([\w,\-\(\) ]*))\s+inst\s+\(([\d\-]+),([\d\-]+)\)')

# Predecessor string format in trace
# Format: src phy(NUMBER) ([livein|ROW,COL])
# ex) src phy(31) (livein)
# ex) src phy(254) (0,2)
pred_rx = re.compile('src\s+[\w\(\)0-9]+\s+\((livein|([\d\-]+),([\d\-]+))\)')

# Register string format (This contains offset format)
# Foramt: rNUMBER | fNUMBER | OFFSET(rNUMBER) | OFFSET(fNUMBER)
# ex) f15
# ex) 0(r20)
reg_rx = re.compile('(r\d+)|[\d-]+\((r\d+)\)|(f\d+)|[\d-]+\((f\d+)\)')

# Register offset format
# Foramt: OFFSET(rNUMBER) | OFFSET(fNUMBER)
# ex) f15
# ex) 0(r20)
reg_offset_rx = re.compile('[\d-]+\((r\d+)\)|[\d-]+\((f\d+)\)')

# I reimplemented this MySet under the false assumption that python 'dict' is
# ordered (There is ordered map elsewhere, actually). But it turns out 'dict'
# is not, and I don't want to change this file to use python set again and
# test everything again...
class MySet:
  def __init__(self, items=[]):
    self.under_map = {}
    for item in items:
      self.add(item)

  def copy(self):
    myset = MySet()
    myset.under_map = copy.copy(self.under_map)
    return myset

  def add(self, item):
    self.under_map[item] = 0

  def add_list(self, item_list):
    for item in item_list:
      self.under_map[item] = 0

  def remove(self, item):
    del self.under_map[item]

  def get(self, item):
    return self.under_map[item]

  def items(self):
    return self.under_map.keys()

  def size(self):
    return len(self.under_map)

  def clear(self):
    self.under_map.clear()

  def has(self, item):
    return self.under_map.has_key(item)

  def union(self, s):
    for item in s.items():
      self.add(item)

  def complement(self, s):
    for item in s.items():
      if self.has(item):
        self.remove(item)

class Instruction:
  # Instruction types. These categories are from
  # gem5-stable/src/cpu/o3/FuncUnitConfig.py
  # We don't use ReadPort, WritePort, SIMD_UNIT, IprAccess categories here.
  IntALU = 0
  IntMulDiv = 1
  FP_ALU = 2
  FP_MultDiv = 3
  RdWrPort = 4

  @staticmethod
  def print_inst_info(inst):
    sys.stdout.write(inst.to_str() + '  |  ')
    for pred in inst.preds:
      sys.stdout.write(pred.to_opcode_str() + ',')
    print

  @staticmethod
  def print_insts_info(insts, desc):
    print desc
    for inst in insts:
      Instruction.print_inst_info(inst)
    print

  def __init__(self, no, inst_str, preds_str, row, col):
    self.no = no
    self.inst_str = inst_str
    self.preds_str = preds_str
    self.opcode = inst_str.split()[0]
    self.row = row
    self.col = col
    self.preds = [] # predecessors
    self.succs = [] # successors
    self.src_regs = []
    self.dst_regs = []
    self.livein_regs = []

  def add_pred(self, pred):
    self.preds.append(pred)

  def add_succ(self, succ):
    self.succs.append(succ)

  def add_src_reg(self, reg):
    self.src_regs.append(reg)

  def add_dst_reg(self, reg):
    # dst reg should not be in offset format like 0(r5)
    assert not reg_offset_rx.match(reg)
    self.dst_regs.append(reg)

  def add_livein_reg(self, reg):
    assert not reg_offset_rx.match(reg)
    self.livein_regs.append(reg)

  def to_opcode_str(self):
    return '%2d %s' % (self.no, self.opcode.ljust(OPCODE_PRINT_WIDTH))

  def to_str(self, opcode_only=False):
    return '%2d %s' % (self.no, self.inst_str)

  def to_info_str(self):
    return self.to_str().ljust(INST_PRINT_WIDTH) + ' | ' + self.preds_str

  def is_load(self):
    return self.opcode.startswith('ld')

class Fab:
  # FU order is the same as that of O3 processor
  # This is the order of categories fixed in the Fabric. Don't change the order
  fu_categories = [Instruction.IntALU, Instruction.IntMulDiv,
                   Instruction.FP_ALU, Instruction.FP_MultDiv,
                   Instruction.RdWrPort]

  def __init__(self, system):
    self.system = system
    # FU setting of DREIT and the cpu should be the same
    num_fus = system.core0.num_IntALUs + system.core0.num_IntMultDivs + \
              system.core0.num_FP_ALUs + system.core0.num_FP_MultDivs + \
              system.core0.num_RdWrPorts
    assert system.core0.fab.num_cols == num_fus
    self.num_rows = system.core0.fab.num_rows
    self.num_cols = system.core0.fab.num_cols

    self.table = [[None for i in range(self.num_cols)]
                  for j in range(self.num_rows)]
    self.max_row = -1
    self.next_row = 0
    self.next_col = 0
    self.opcode_to_category = {}
    self.set_fu_columns(system)

  def set_fu_columns(self, system):
    # (FU, schedulable columns) map
    fu_list = Fab.fu_categories
    num_fu_list = [system.core0.num_IntALUs, system.core0.num_IntMultDivs,
                   system.core0.num_FP_ALUs, system.core0.num_FP_MultDivs,
                   system.core0.num_RdWrPorts]

    pos = 0
    self.fu_cols = {}
    for (fu, num_fu) in zip(fu_list, num_fu_list):
      self.fu_cols[fu] = range(pos, pos + num_fu)
      pos += num_fu

  # It would be too much work to parse the actual arch ISA table. Here we just
  # use the fab scheduling information to infer instruction category.
  def add_opcode_category(self, inst, col):
    categorized = False
    for category in Fab.fu_categories:
      if col in self.fu_cols[category]:
        self.opcode_to_category[inst.opcode] = category
        categorized = True
        break
    assert categorized

  def get_opcode_category(self, inst):
    return self.opcode_to_category[inst.opcode]

  def copy(self):
    fab = Fab(self.system)
    fab.table = [[None for i in range(self.num_cols)]
                 for j in range(self.num_rows)]
    for row in range(self.num_rows):
      for col in range(self.num_cols):
        fab.table[row][col] = self.table[row][col]
    fab.max_row = self.max_row
    fab.num_rows = self.num_rows
    fab.num_cols = self.num_cols
    fab.next_row = self.next_row
    fab.next_col = self.next_col
    return fab

  def schedule(self, inst, row, col):
    self.table[row][col] = inst
    if self.max_row < row:
      self.max_row = row

  def can_schedule_next(self, inst):
    opcode_category = self.get_opcode_category(inst)
    is_match_category = self.next_col in self.fu_cols[opcode_category]
    is_slot_empty = not self.table[self.next_row][self.next_col]
    return is_match_category and is_slot_empty

  def reset_next_slot(self, row=0, col=0):
    self.next_row = row
    self.next_col = col

  def increment_next_slot(self):
    self.next_col += 1
    if self.next_col == self.num_cols:
      self.next_col = 0
      self.next_row += 1

  def schedule_next(self, inst):
    assert self.next_row < self.num_rows
    assert self.next_col < self.num_cols
    (row, col) = (self.next_row, self.next_col)
    self.schedule(inst, self.next_row, self.next_col)
    self.increment_next_slot()
    return (row, col)

  def get(self, row, col):
    return self.table[row][col]

  def prettyprint(self):
    print '   ',
    for j in range(self.num_cols):
      sys.stdout.write(str(j).rjust(OPCODE_PRINT_WIDTH + 2) + ' |')
    print
    for i in range(self.num_rows):
      print '%2d:' % i,
      for j in range(self.num_cols):
        inst = self.get(i, j)
        if not inst:
          sys.stdout.write('   ' + ' ' * OPCODE_PRINT_WIDTH + '|')
        else:
          sys.stdout.write(inst.to_opcode_str().ljust(OPCODE_PRINT_WIDTH) + '|')
      print

class Schedule:
  def __init__(self, trace_idx, entry_idx, system=None, fab=None):
    self.trace_idx = trace_idx
    self.entry_idx = entry_idx
    # Fab is already given
    if fab:
      self.fab = fab
    else:
      assert system
      self.fab = Fab(system)
    self.insts = [] # should be in program order
    self.dep_graph = None # Will be created as needed

  def get_system(self):
    return self.fab.system

  def add_inst(self, inst, row, col):
    self.fab.schedule(inst, row, col)
    self.insts.append(inst)
    self.fab.add_opcode_category(inst, col)

  def get_inst(self, row, col):
    return self.fab.get(row, col)

  def get_opcode_category_info(self):
    return self.fab.opcode_to_category

  def get_height(self):
    return self.fab.max_row + 1

  def prettyprint(self):
    self.fab.prettyprint()

  def get_livein_regs(self):
    livein_set = MySet()
    for inst in self.insts:
      livein_set.add_list(inst.livein_regs)
    return livein_set.items()

  # This is different from what we usually call 'liveness analysis'
  # We don't 'kill' values; we only generate values, because we don't know
  # which values will be used afterwards anyway.
  def get_liveout_regs(self):
    liveout_set = MySet()
    for inst in self.insts:
      liveout_set.union(MySet(inst.dst_regs))
    return liveout_set.items()

  # Tries to schedule instructions using list scheduling.
  @staticmethod
  def create_list_schedule(insts, system, opcode_to_category, option):
    dep_graph = DependenceGraph(insts)
    fab = Fab(system)
    fab.opcode_to_category = opcode_to_category

    while not dep_graph.is_all_inst_scheduled():
      ready_inst = dep_graph.get_next_ready_inst(option)
      fab.reset_next_slot(dep_graph.get_earliest_schedule_row(ready_inst), 0)
      while not fab.can_schedule_next(ready_inst):
        fab.increment_next_slot()
      (row, col) = fab.schedule_next(ready_inst)
      dep_graph.mark_scheduled(ready_inst, row)

    list_schedule = Schedule(-1, -1, system, fab) # No trace/index number
    list_schedule.fab = fab
    list_schedule.dep_graph = dep_graph
    return list_schedule

  # Tries to create an optimal schedule using try all combinations possible.
  # Complexity O(n!). Does not work for n greater than 10 I guess.
  @staticmethod
  def create_optimal_schedule(insts, system, opcode_to_category):
    dep_graph = DependenceGraph(insts)
    fab = Fab(system)
    fab.opcode_to_category = opcode_to_category

    optimal_fab = Schedule.search_optimal_fab(fab, dep_graph)
    optimal_schedule = Schedule(-1, -1, system, fab) # No trace/index number
    optimal_schedule.fab = optimal_fab
    optimal_schedule.dep_graph = dep_graph
    return optimal_schedule

  @staticmethod
  def search_optimal_fab(fab, dep_graph):
    if dep_graph.is_all_inst_scheduled():
      return fab

    fabs = []
    scheduled = False
    for ready_inst in dep_graph.get_ready_insts(fab.next_row):
      if fab.can_schedule_next(ready_inst):
        new_fab = fab.copy()
        new_dep_graph = dep_graph.copy()
        (row, col) = new_fab.schedule_next(ready_inst)
        new_dep_graph.mark_scheduled(ready_inst, row)
        fabs.append(Schedule.search_optimal_fab(new_fab, new_dep_graph))
        scheduled = True
    if not scheduled: # no ready instruction can be scheduled in this slot
      new_fab = fab.copy()
      new_fab.increment_next_slot()
      new_dep_graph = dep_graph.copy()
      fabs.append(Schedule.search_optimal_fab(new_fab, new_dep_graph))

    # Return the schedule with the smallest height
    min_height = sys.maxint
    min_fab = None
    for fab in fabs:
      if fab.height < min_height:
        min_height = fab.height
        min_fab = fab
    return fab

  # This checks if schedules produced by the program is near-optimial;
  # every instruction should be scheduled in the row that is immediately below
  # the row its latest predecessor is scheduled, unless that row has no
  # available functional units.
  def check_efficient_schedule(self):
    nonefficient_insts = []
    for i in range(self.fab.num_rows):
      for j in range(self.fab.num_cols):
        inst = self.get_inst(i, j)
        if not inst: # No instruction is scheduled in this slot
          continue

        max_pred_row = -1
        for pred in inst.preds:
          if pred.row > max_pred_row:
            max_pred_row = pred.row

        if inst.row != max_pred_row + 1:
          nonefficient_insts.append(((inst.row, max_pred_row+1), inst))
    if len(nonefficient_insts) > 0:
      self.report_nonefficient_schedule(nonefficient_insts)

  def report_nonefficient_schedule(self, nonefficient_insts):
    print
    print '* Schedule: trace_idx = %d  entry_idx = %d' % \
          (self.trace_idx, self.entry_idx)
    self.prettyprint()
    print
    print '* Non-efficient Instructions:'
    print 'Current  Optimal    Instruction'
    for ((cur_row, opt_row), inst) in nonefficient_insts:
      print '%7d  %7d    %s' % (cur_row, opt_row, inst.to_info_str())
    print

class DependenceGraph:
  # When multiple instructions are ready at the same time, pick one with a
  # greatest height (= one in the critical path)
  CRITICAL_PATH_FIRST = 0
  # When multiple instructions are ready at the same time, pick one which
  # appears earliest in the trace
  PROGRAM_ORDER_FIRST = 1

  def __init__(self, insts=[]):
    self.insts = insts
    self.unsched_inst_set = MySet(insts)
    self.ready_inst_set = MySet()

    # (instruction, list of its unscheduled predecessors) map
    self.unsched_pred_sets = {}

    for inst in insts:
      self.unsched_pred_sets[inst] = MySet()
      if len(inst.preds) == 0:
        self.ready_inst_set.add(inst)
      else:
        self.unsched_pred_sets[inst] = MySet(inst.preds)

    self.inst_succ_depths = {} # depth of successors
    for inst in self.insts:
      self.inst_succ_depths[inst] = self.get_inst_succ_depths(inst)

    self.inst_sched_heights = {} # Instruction scheduling heights

  def get_inst_succ_depths(self, inst):
    if self.inst_succ_depths.has_key(inst): # already computed
      return self.inst_succ_depths[inst]
    if len(inst.succs) == 0:
      self.inst_succ_depths[inst] = 0
      return 0

    succ_depths = []
    for succ in inst.succs:
      succ_depths.append(self.get_inst_succ_depths(succ))
    height = max(succ_depths) + 1
    self.inst_succ_depths[inst] = height
    return height

  def get_critical_path_len(self):
    max_succ_inst = max(self.inst_succ_depths, key=self.inst_succ_depths.get)
    return self.inst_succ_depths[max_succ_inst] + 1

  def print_ready_insts(self):
    print
    title = 'Ready Instructions:'
    Instruction.print_insts_info(self.ready_inst_set.items(), title)

  def print_unsched_insts(self):
    print
    title = 'Unscheduled Instructions:'
    Instruction.print_insts_info(self.unsched_inst_set.items(), title)

  def get_next_ready_inst(self, option):
    # Returns an instruction with highest successor depth - which means it is on
    # the critical path
    if option == DependenceGraph.CRITICAL_PATH_FIRST:
      max_succ_depth = -1
      max_succ_inst = None
      for inst in self.ready_inst_set.items():
        if max_succ_depth < self.inst_succ_depths[inst]:
          max_succ_depth = self.inst_succ_depths[inst]
          max_succ_inst = inst
      return max_succ_inst

    # Returns an instruction with the earliest program order first - which
    # means it appears first in the trace
    elif option == DependenceGraph.PROGRAM_ORDER_FIRST:
      for inst in self.insts:
        if inst in self.ready_inst_set.items():
          return inst

  # Returns the earliest possible row(cycle) this instruction can be scheduled
  # The given instruction should be ready (all of its predecessors have been
  # scheduled)
  def get_earliest_schedule_row(self, inst):
    assert inst in self.ready_inst_set.items()
    if len(inst.preds) == 0:
      return 0

    max_pred_height = -1
    for pred in inst.preds:
      if self.inst_sched_heights[pred] > max_pred_height:
        max_pred_height = self.inst_sched_heights[pred]
    return max_pred_height + 1

  def copy(self):
    dep_graph = DependenceGraph()
    dep_graph.insts = copy.copy(self.insts)
    dep_graph.unsched_inst_set = self.unsched_inst_set.copy()
    dep_graph.ready_inst_set = self.ready_inst_set.copy()
    dep_graph.unsched_pred_sets = {}
    for inst in self.unsched_pred_sets.keys():
      dep_graph.unsched_pred_sets[inst] = self.unsched_pred_sets[inst].copy()
    dep_graph.inst_succ_depths = copy.copy(self.inst_succ_depths)
    dep_graph.inst_sched_heights = copy.copy(self.inst_sched_heights)
    return dep_graph

  def get_ready_insts(self, row):
    ready_insts = []
    for inst in self.ready_inst_set.items():
      if self.get_earliest_schedule_row(inst) <= row:
        ready_insts.append(inst)
    return ready_insts

  def mark_scheduled(self, sched_inst, row):
    self.unsched_inst_set.remove(sched_inst)
    self.ready_inst_set.remove(sched_inst)
    self.inst_sched_heights[sched_inst] = row

    for inst in self.unsched_inst_set.items():
      if sched_inst in self.unsched_pred_sets[inst].items():
        self.unsched_pred_sets[inst].remove(sched_inst)
        if self.unsched_pred_sets[inst].size() == 0:
          self.ready_inst_set.add(inst)

  def is_all_inst_scheduled(self):
    return self.unsched_inst_set.size() == 0

class Benchmark:
  def __init__(self, name):
    self.name = name
    self.schedules = []

  def add_schedule(self, schedule):
    self.schedules.append(schedule)


def read_benchmark_schedules(benchmark_path, benchmark_names, trace_file,
                             conf_file, stat_file):
  benchmarks = []

  for bench_name in benchmark_names:
    trace_path = benchmark_path + bench_name + '/' + trace_file
    conf_path = benchmark_path + bench_name + '/' + conf_file
    stat_path = benchmark_path + bench_name + '/' + stat_file
    for f in [trace_path, conf_path, stat_path]:
      if not os.path.isfile(f):
        sys.stderr.write('Cannot open file: ' + f + '\n')
        sys.exit(1)

    print 'Processing: ' + benchmark_path + bench_name

    core0_name = 'cpu' # For DREIT, it is always 'cpu'
    confs = gem5_to_mcpat.parse_conf_file(conf_path)
    stats = gem5_to_mcpat.parse_stat_file(stat_path)
    system_name = confs['root']['children'].split()[0]
    system = gem5_to_mcpat.System(system_name, None, confs, stats, core0_name)

    benchmark = Benchmark(bench_name)
    benchmarks.append(benchmark)

    with open(trace_path) as fp:
      in_schedule = False
      schedule = None
      inst_no = -1

      for line in fp:
        if not fab_prefix in line:
          continue

        # Remove prefix
        info_str = line_rx.match(line).group(1)

        sched_prolog_m = sched_prolog_rx.match(info_str)
        if sched_prolog_m:
          in_schedule = True
          inst_no = 0
          trace_idx = int(sched_prolog_m.group(1))
          entry_idx = int(sched_prolog_m.group(2))
          schedule = Schedule(trace_idx, entry_idx, system)
          continue

        if not in_schedule:
          continue

        # ex) bis        r31,r2,r12    inst (1,1)
        inst_info_str = info_str[:info_str.find('|')].strip()
        # ex) src phy(31) (livein)    src phy(254) (0,2)
        preds_str = info_str[info_str.find('|')+1:].strip()

        inst_m = inst_rx.match(inst_info_str)

        if not inst_m: # Schedule finished
          in_schedule = False
          inst_no = -1
          benchmark.add_schedule(schedule)
          continue

        inst_str = inst_m.group(1).strip() # bis        r31,r2,r12
        regs_str = inst_m.group(2).strip() # r31,r2,r12
        row = int(inst_m.group(3))
        col = int(inst_m.group(4))
        if row == -1 and col == -1: # Unscheduled instruction (uncond br)
          continue

        # Parse register string
        regs = []
        for reg in regs_str.split(','):
          reg_m = reg_rx.match(reg)
          if reg_m:
            if reg_m.group(1): # ex) r0
              regs.append(reg_m.group(1))
            elif reg_m.group(2): # ex) 10(r0)
              regs.append(reg_m.group(2))
            elif reg_m.group(3): # ex) f5
              regs.append(reg_m.group(3))
            elif reg_m.group(4): # ex) 4(f5)
              regs.append(reg_m.group(4))

        inst = Instruction(inst_no, inst_str, preds_str, row, col)
        schedule.add_inst(inst, row, col)
        inst_no += 1

        # HACK Feng says, only for ld** instructions, the destination register
        # comes first...
        idx = 0
        if inst.is_load():
          inst.add_dst_reg(regs[idx])
          idx += 1

        for pred_m in pred_rx.findall(preds_str):
          inst.add_src_reg(regs[idx])
          if pred_m[0] == 'livein':
            inst.add_livein_reg(regs[idx])
          else:
            pred = schedule.get_inst(int(pred_m[1]), int(pred_m[2]))
            inst.add_pred(pred)
            pred.add_succ(inst)
          idx += 1

        assert(len(regs[idx:]) <= 1) # number of dst should be 0 or 1
        if len(regs[idx:]) == 1:
          inst.add_dst_reg(regs[idx])

  print
  return benchmarks

def print_schedule_counts(benchmarks):
  print 'Number of created schedules:'
  for benchmark in benchmarks:
    print '%s   %4d' % (benchmark.name.ljust(15), len(benchmark.schedules))
  print

def check_benchmark_schedules(benchmarks):
  for benchmark in benchmarks:
    print
    print '--  Checking ' + benchmark.name + ':'
    for schedule in benchmark.schedules:
      schedule.check_efficient_schedule()
    print

def print_static_schedules(benchmarks):
  summary = {}
  for benchmark in benchmarks:
    summary[benchmark] = []

    print
    print '-- ' + benchmark.name + ':'

    for schedule in benchmark.schedules:
      print '* Schedule: trace_idx = %d  entry_idx = %d' % \
            (schedule.trace_idx, schedule.entry_idx)
      print
      print '** Original schedule: height = %d' % schedule.get_height()
      schedule.prettyprint()
      print

      # Program order method
      program_schedule = Schedule.create_list_schedule(
          schedule.insts, schedule.get_system(),
          schedule.get_opcode_category_info(),
          DependenceGraph.PROGRAM_ORDER_FIRST)
      crit_len = program_schedule.dep_graph.get_critical_path_len()

      s = '** Program order first schedule: height = %d ' \
          '(Critical path length = %d)' % \
          (program_schedule.get_height(), crit_len)
      print s
      program_schedule.prettyprint()
      print

      # Critical path method
      crit_schedule = Schedule.create_list_schedule(
          schedule.insts, schedule.get_system(),
          schedule.get_opcode_category_info(),
          DependenceGraph.CRITICAL_PATH_FIRST)
      s = '** Critical path first schedule: height = %d ' \
          '(Critical path length = %d)' % \
          (crit_schedule.get_height(), crit_len)
      print s
      crit_schedule.prettyprint()

      summary[benchmark].append((schedule.trace_idx, schedule.entry_idx,
          schedule.get_height(), program_schedule.get_height(),
          crit_schedule.get_height(), crit_len))
      print
      print
    print

  print 'Summary:'
  print '  trace_idx: trace index'
  print '  entry_idx: entry index'
  print '  height: DREIT schedule height'
  print '  p.height: program order first schedule height'
  print '  c.height: critical path first schedule height'
  print '  crit_len: critical path length'
  print
  print 'trace_idx  entry_idx  height  p.height  c.height  crit_len'
  for benchmark in summary.keys():
    print '- ' + benchmark.name
    for item in summary[benchmark]:
      print '%9d  %9d  %6d  %8d  %8d  %8d' % item

def print_max_schedule_lengths(benchmarks):
  print 'Schedule lengths:'
  for benchmark in benchmarks:
    max_len = -1
    for schedule in benchmark.schedules:
      if max_len < schedule.get_height():
        max_len = schedule.get_height()
    print '%s   %4d' % (benchmark.name.ljust(15), max_len)
  print

def print_livein_liveout_info(benchmarks):
  print 'Average number of liveins and liveouts:'
  print 'benchmark       livein   liveout'
  for benchmark in benchmarks:
    num_liveins = []
    num_liveouts = []
    for schedule in benchmark.schedules:
      num_liveins.append(len(schedule.get_livein_regs()))
      num_liveouts.append(len(schedule.get_liveout_regs()))
    avg_num_liveins = float(sum(num_liveins)) / len(num_liveins)
    avg_num_liveouts = float(sum(num_liveouts)) / len(num_liveouts)
    print '%s   %4.1f      %4.1f' % \
          (benchmark.name.ljust(15), avg_num_liveins, avg_num_liveouts)
