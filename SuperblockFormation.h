// This is a part of code I wrote as a TA for COS320 "Compiling Techniques"
// course in Spring 2015. It was the reference solution of one of the
// assignments.
// This is an LLVM pass that performs Superblock formation.

#ifndef FUN_SUPERBLOCK_SUPERBLOCKFORMATION_H
#define FUN_SUPERBLOCK_SUPERBLOCKFORMATION_H

#include "Profile/BBProfileLoader.h"
#include "Profile/EdgeProfileLoader.h"
#include "llvm/Pass.h"
#include "llvm/ADT/iterator_range.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/SSAUpdater.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Support/raw_ostream.h"
#include <deque>

namespace fun {

using namespace llvm;

class SBTrace {
public:
  typedef std::deque<BasicBlock*>::iterator iterator;
  typedef std::deque<BasicBlock*>::const_iterator const_iterator;

  SBTrace(BasicBlock *seed, Loop *loop)
    : loop(loop), dupStartBB(nullptr), dupEndBB(nullptr) {
      bbq.push_back(seed);
  }

  Loop *getLoop() const { return loop; }

  void pushFront(BasicBlock* bb) { bbq.push_front(bb); }
  void pushBack(BasicBlock* bb) { bbq.push_back(bb); }
  BasicBlock *front() { return bbq.front(); }
  bool isFront(const BasicBlock *bb) const { return bbq.front() == bb; }
  BasicBlock *back() { return bbq.back(); }
  bool isBack(const BasicBlock *bb) const { return bbq.back() == bb; }

  unsigned size() const { return bbq.size(); }
  bool contains(const BasicBlock *bb) const {
    return std::find(bbq.begin(), bbq.end(), bb) != bbq.end();
  }

  BasicBlock *getDupStartBB() const { return dupStartBB; }
  void setDupStartBB(BasicBlock *bb) { dupStartBB = bb; }
  BasicBlock *getDupEndBB() const { return dupEndBB; }
  void setDupEndBB(BasicBlock *bb) { dupEndBB = bb; }
  bool hasDupBlocks() const { return dupStartBB != nullptr; }

  iterator bb_begin() { return bbq.begin(); }
  const_iterator bb_begin() const { return bbq.begin(); }
  iterator bb_end() { return bbq.end(); }
  const_iterator bb_end() const { return bbq.end(); }
  iterator_range<iterator> bbs() {
    return iterator_range<iterator>(bb_begin(), bb_end());
  }
  iterator_range<const_iterator> bbs() const {
    return iterator_range<const_iterator>(bb_begin(), bb_end());
  }

  iterator dupBB_begin() {
    return dupStartBB ?
           std::find(bbq.begin(), bbq.end(), dupStartBB) : bb_end();
  }
  const_iterator dupBB_begin() const {
    return dupStartBB ?
           std::find(bbq.begin(), bbq.end(), dupStartBB) : bb_end();
  }
  iterator dupBB_end() {
    return dupEndBB ?
           std::find(bbq.begin(), bbq.end(), dupEndBB) : bb_end();
  }
  const_iterator dupBB_end() const {
    return dupEndBB ?
           std::find(bbq.begin(), bbq.end(), dupEndBB) : bb_end();
  }
  iterator_range<iterator> dupBBs() {
    return iterator_range<iterator>(dupBB_begin(), dupBB_end());
  }
  iterator_range<const_iterator> dupBBs() const {
    return iterator_range<const_iterator>(dupBB_begin(), dupBB_end());
  }

  BasicBlock *prev(const BasicBlock *bb) {
    return bb == front() ? nullptr : *--std::find(bbq.begin(), bbq.end(), bb);
  }
  BasicBlock *next(const BasicBlock *bb) {
    return bb == back() ? nullptr : *++std::find(bbq.begin(), bbq.end(), bb);
  }

  void print(raw_ostream &out) const;
  void dump() const;

private:
  Loop *loop;
  std::deque<BasicBlock*> bbq;
  BasicBlock *dupStartBB; // inclusive
  BasicBlock *dupEndBB; // exclusive
};

class SuperblockFormation : public ModulePass {
public:
  typedef DenseSet<BasicBlock*> BBSetTy;
  typedef std::vector<Loop*> LoopVecTy;
  typedef std::map<Loop*, std::vector<SBTrace*>> LoopToTracesMapTy;

  static char ID; // Pass identification, replacement for typeid

  SuperblockFormation() : ModulePass(ID) {}

  bool runOnModule(Module &m);

  virtual const char *getPassName() const override {
    return "Superblock formation";
  }

  void getAnalysisUsage(AnalysisUsage &au) const override {
    au.addRequired<LoopInfo>();
    au.addRequired<BBProfileLoader>();
    au.addRequired<EdgeProfileLoader>();
  }

private:
  void getInnermostLoops(Loop *l, LoopVecTy &innermostLoops) const;
  void collectTrace(Loop *l, LoopToTracesMapTy &loop2Traces);

  void growTraceForward(SBTrace *trace, BBSetTy &visitedBBs);
  void growTraceBackward(SBTrace *trace, BBSetTy &visitedBBs);

  BasicBlock *getBestBB(const Loop *l, const BBSetTy &visitedBB) const;
  BasicBlock *getBestPred(BasicBlock *bb, const Loop *l,
                          const BBSetTy &visitedBB) const;
  BasicBlock *getBestSucc(BasicBlock *bb, const Loop *l,
                          const BBSetTy &visitedBB) const;

  void duplicateTail(SBTrace *trace, unsigned maxCodeSize,
                     unsigned &curCodeSize);

  void figureOutBlocksToDuplicate(
      SBTrace *trace, unsigned maxCodeSize, unsigned &curCodeSize) const;
  void cloneBBs(SBTrace *trace, ValueToValueMapTy &vmap);
  void fixSideEntrances(SBTrace *trace, const ValueToValueMapTy &vmap);
  void fixPHINodes(SBTrace *trace, const ValueToValueMapTy &vmap);
  void updateSSA(SBTrace *trace, ValueToValueMapTy &vmap);

  // Helper functions for tail duplication
  unsigned getNumNonPHIInsts(const BasicBlock *bb) const;
  bool hasSideEntrance(const BasicBlock *bb, SBTrace *trace) const;
  void remapInstruction(Instruction *i, ValueToValueMapTy &vmap);
  void rewriteUse(SSAUpdater &ssa, Use &u);
  bool isBeforeInBB(const BasicBlock *bb, const Instruction *i1,
                    const Instruction *i2) const;

  BBProfileLoader *bbLoader;
  EdgeProfileLoader *edgeLoader;
};

}

#endif
