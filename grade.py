# This is a part of code I wrote as a TA for COS320 "Compiling Techniques"
# course in Spring 2015.
# This is a Python program used to automatically grade assignments.

# This script should run in grading/.

import os
import sys
import re
import glob
import filecmp
import difflib
import functools
import numpy

# Change this to TA's NetID next semster!
my_netid = 'heejin'

# import dropbox scripts (for required_files and grading_copy_list)
num_assignments = 6
sys.path.append('../dropbox/scripts/')
import test_fmk
import util
for i in range(1, num_assignments+1):
  exec('import AS' + str(i))

# Data for the semester
sys.path.append('data_' + test_fmk.year_semester() + '/others/')
import latedays
import misc_points
import misc_deductions

# If true, run 'make clean' after grading every student.
# Making this 'True' is safer, but extremely time-consuming when testing.
# So I suggest to turn this on only when generating final grading reports.
make_clean_everytime = True

# Output postprocessing on/off - should be on for generating reports, but
# sometimes need to be off for testing
postprocess = True

# submit.modified is where modified submitted files exist.
# I wanted to make outputs diff-able by using all those regex and stuff to
# automate everything, but sometimes I need to modify students' code a bit
# to make the autograding work. So submit/ contains what students originally
# submitted, and submit.modified/ contains the modified version I made
# (Even if there is fortunately no modifications, I use the 'modified'
# directory)
submit_path = 'data_' + test_fmk.year_semester() + '/submit.modified/'
output_path = 'data_' + test_fmk.year_semester() + '/output/'
if not postprocess:
  output_path = 'data_' + test_fmk.year_semester() + '/output.nopp/'
tests_path = 'tests/'

make_cmd_str = 'make -j%d' % util.get_cpu_count()
ls_cmd_str = 'ls -ogh'

# Directory under submit/as?/ that contains reference output
reference_dir = '__reference'

# Grading report filename
report_file = 'report.txt'

# Test directory description file (existing in each test subdirectory)
test_dir_desc_file = 'desc.txt'

# Files to save 'make' and 'ls''s output. Underpoints prefixes are to prevent
# filename clashes when there is a test case named like 'make.fun'.
make_out_base = '__make'
ls_out_base = '__ls'

# Max score for an assignment
max_score = 100

# Class that represents one grading test setting
class GradeSetup:
  SHOULD_BE_ZERO = 0
  SHOULD_BE_NONZERO = 1
  DONT_CARE = 2
  # test_dir: test subdirectory
  # tool: test tool binary
  # points: points allocated to this test
  # batch: whether it is a batch tool (that runs on all the files in a dir) or
  #        a non-batch tool (that runs on each file separately)
  # exit_code: one of SHOULD_BE_ZERO / SHOULD_BE_NONZERO / DONT_CARE
  # out/err_processing_funcs: postprocessing functions
  # out_dir_opt: if specified, output directory will be given as an option to
  #              the command line
  # opts: custom options to the tool
  # input_exts: input file extension. defaults to 'fun' because AS1-AS4 uses it
  def __init__(
      self,
      test_dir,
      tool,
      points,
      batch,
      exit_code=SHOULD_BE_ZERO,
      out_postprocess_funcs=[],
      err_postprocess_funcs=[],
      out_ext='out',
      err_ext='err',
      out_dir_opt = '',
      opts='',
      input_exts=['fun']):
    self.test_dir = test_dir
    self.tool = tool
    self.points = points
    self.batch = batch
    self.exit_code = exit_code
    self.out_postprocess_funcs = out_postprocess_funcs
    self.err_postprocess_funcs = err_postprocess_funcs
    self.out_ext = out_ext
    self.err_ext = err_ext
    self.out_dir_opt = out_dir_opt
    self.opts = opts
    self.input_exts = input_exts

# This should be imported here because of circular dependency between
# grade.py and grade_setup.py. This is bad..
import grade_setup

# Every test case file is supposed to have this kind of line at the top:
# /* Description: [TEST DESCRIPTION GOES HERE] */
# In case the input file is an .ll file, the form will be
# ; Description: [TEST DESCRIPTION GOES HERE]
# Search for line containing this and returns the description string.
# If the pattern does not exist, returns the filename.
def get_test_file_description(test_file):
  # When the test file is a directory - this means this is a benchmark directory
  # In that case, just return its name
  if os.path.isdir(test_file):
    return os.path.basename(os.path.normpath(test_file))

  if test_file.endswith('.ll'):
    test_desc_rx = re.compile('\s*;\s*Description\s*:\s*(.*)\s*$')
  elif test_file.endswith('.c') or test_file.endswith('.fun'):
    test_desc_rx = re.compile('\s*/\*\s*Description\s*:\s*(.*)\s*\*/')
  else:
    sys.stderr.write('Unexpected test file extension: ' + test_file + '\n')
    sys.exit(1)

  with open(test_file) as fp:
    for line in fp:
      m = test_desc_rx.match(line)
      if m:
        return m.group(1).strip()
  return os.path.basename(test_file)

# Returns a directory description by reading "desc.txt" in the directory.
def get_test_dir_description(test_dir):
  desc_file = test_dir + '/' + test_dir_desc_file
  if not os.path.isfile(desc_file):
    sys.stderr.write(desc_file + ' does not exist\n')
    sys.exit(1)
  f = open(test_dir + '/' + test_dir_desc_file, 'r')
  desc = f.read()
  f.close()
  return desc.strip()

## Path helper functions

# ex) submit/as1/
def get_as_submit_path(as_no):
  return submit_path + 'as' + str(as_no) + '/'

# ex) tests/as1/
def get_as_tests_path(as_no):
  return tests_path + 'as' + str(as_no) + '/'

# ex) output/as1/
def get_as_output_path(as_no):
  return output_path + 'as' + str(as_no) + '/'

# ex) submit/as1/NETID/
def get_student_submit_path(as_no, netid):
  return get_as_submit_path(as_no) + netid + '/'

# ex) output/as1/NETID/
def get_student_output_path(as_no, netid):
  return get_as_output_path(as_no) + netid + '/'

# ex) output/as1/__reference/
def get_ref_output_path(as_no, netid):
  return get_student_output_path(as_no, reference_dir)

# ex) testbed/as1/student/
def get_student_build_path(as_no):
  # Unlike dropbox, we share one build dir for all students because there is no
  # risk of racial condition
  return test_fmk.testbed_path + 'as' + str(as_no) + '/__student/'

# ex) output/as1/NETID/report.txt
def get_report_path(as_no, netid):
  return get_student_output_path(as_no, netid) + report_file

# Get appropriate dropbox script module
def get_as_module(as_no):
  return eval('AS' + str(as_no))

# ex) tests/as1/a/b/c.fun -> output/as1/NETID/a/b/c.[out/err]
def get_corresponding_out_file(as_no, netid, test_file, out_ext):
  file_in_out_dir = util.get_output_filename(get_as_tests_path(as_no),
                    get_student_output_path(as_no, netid), test_file)
  out_file = util.change_or_add_extension(file_in_out_dir, out_ext)
  return os.path.abspath(out_file)

# ex) output/as1/NETID/__make.[out/err]
def get_make_out_file(as_no, netid, out_ext):
  out_file = get_student_output_path(as_no, netid) + make_out_base + '.' + \
             out_ext
  return os.path.abspath(out_file)

# ex) output/as1/NETID/__ls.out
def get_ls_out_file(as_no, netid):
  out_file = get_student_output_path(as_no, netid) + ls_out_base + '.out'
  return os.path.abspath(out_file)

# Generate command string for a testing tool
def get_cmd_str(as_no, netid, test_file, setup):
  cmd_str = setup.tool + ' ' + os.path.abspath(test_file)

  # In case if the tool has a separate 'output directory' option
  # ex) utils/codegen.py in AS4
  if setup.out_dir_opt != '':
    dummy_out_file = \
      get_corresponding_out_file(as_no, netid, test_file, 'dummy')
    out_dir = os.path.abspath(os.path.dirname(dummy_out_file))
    cmd_str += ' ' + setup.out_dir_opt + ' ' + out_dir + ' ' + setup.opts
  return cmd_str

## General-purpose output generation functions

# Generate 'ls' output in student submit directory
def generate_ls_output(as_no, netid):
  util.ensure_path(get_student_output_path(as_no, netid))
  out_f = open(get_ls_out_file(as_no, netid), 'w')
  abs_submit_path = os.path.abspath(get_student_submit_path(as_no, netid))
  ls_cmd = test_fmk.TestCommand(ls_cmd_str, dir=abs_submit_path, stdout=out_f,
           stderr=None, print_cmd=False, stop_on_failure=True)
  exit_codes = test_fmk.do_test(
      as_no,
      test_fmk.grading_mode,
      get_student_submit_path(as_no, netid),
      get_student_build_path(as_no),
      get_as_module(as_no).required_files,
      get_as_module(as_no).grading_copy_list,
      [ls_cmd],
      False)
  out_f.close()
  return exit_codes

# Generate 'make' output
def generate_make_output(as_no, netid):
  util.ensure_path(get_student_output_path(as_no, netid))
  out_f = open(get_make_out_file(as_no, netid, 'out'), 'w')
  err_f = open(get_make_out_file(as_no, netid, 'err'), 'w')
  make_cmd = test_fmk.TestCommand(make_cmd_str, stdout=out_f, stderr=err_f,
                              print_cmd=False, stop_on_failure=True)
  exit_codes = test_fmk.do_test(
      as_no,
      test_fmk.grading_mode,
      get_student_submit_path(as_no, netid),
      get_student_build_path(as_no),
      get_as_module(as_no).required_files,
      get_as_module(as_no).grading_copy_list,
      [make_cmd],
      False)
  out_f.flush()
  err_f.flush()
  out_f.close()
  err_f.close()

  # Delete 'heejin (my netid)' from the compilation error messages, because
  # these error messages will be included in the grading report
  remove_lines_with_word(get_make_out_file(as_no, netid, 'err'), my_netid)
  return exit_codes

# Generate outputs for single command-line tools, as in as1-as4
# test_dir: specific subdirectory you want to test in tests/as?/
# out_ext: file extension for output-redicrected file
# run: if False, will not run tests actually; this will just generate objects
#      of 'TestCommand' and set exit codes to -1
def generate_single_tool_output(as_no, netid, setup, run=True):
  cmds = []
  # We gather open file output streams here to close() them at the end
  fstreams = []
  test_files = []
  if setup.batch:
    flist = glob.glob(get_as_tests_path(as_no) + setup.test_dir + '/*')
    test_files = filter(os.path.isdir, flist)
  else:
    for ext in setup.input_exts:
      test_files += util.get_all_files_recurse(get_as_tests_path(as_no) +
                                               setup.test_dir, ext)

  for test_file in test_files:
    cmd_str = get_cmd_str(as_no, netid, test_file, setup)
    out_file = \
      get_corresponding_out_file(as_no, netid, test_file, setup.out_ext)
    err_file = \
      get_corresponding_out_file(as_no, netid, test_file, setup.err_ext)

    util.ensure_path(os.path.dirname(out_file))
    out_f = open(out_file, 'w')
    err_f = open(err_file, 'w')

    cmd = test_fmk.TestCommand(cmd_str, stdout=out_f, stderr=err_f,
                               print_cmd=False, stop_on_failure=False)
    cmds.append(cmd)
    fstreams.append(out_f)
    fstreams.append(err_f)

  # Run commands actually
  exit_codes = {}
  if run:
    exit_codes = test_fmk.do_test(
        as_no,
        test_fmk.grading_mode,
        get_student_submit_path(as_no, netid),
        get_student_build_path(as_no),
        get_as_module(as_no).required_files,
        get_as_module(as_no).grading_copy_list,
        cmds,
        False)
  else:
    for cmd in cmds:
      exit_codes[cmd] = -1
  for stream in fstreams:
    stream.close()

  # Apply postprocessing functions
  # ex) deleting some student-specific error messages, ...
  for test_file in test_files:
    out_file = \
      get_corresponding_out_file(as_no, netid, test_file, setup.out_ext)
    err_file = \
      get_corresponding_out_file(as_no, netid, test_file, setup.err_ext)
    if postprocess:
      for func in setup.out_postprocess_funcs:
        func(out_file)
      for func in setup.err_postprocess_funcs:
        func(err_file)

  return exit_codes

# Output generation function
def generate_output(as_no, netid):
  generate_ls_output(as_no, netid)
  make_exit_codes = generate_make_output(as_no, netid)
  make_exit_code = make_exit_codes.values()[0]
  if netid == reference_dir: # Reference should compile
    if make_exit_code != 0:
      sys.stderr.write('Reference\'s build failed\n')
      sys.exit(1)

  grade_setups = eval('grade_setup.get_grade_setups_as' + str(as_no))()
  test_exit_codes = {}
  for setup in grade_setups:
    codes = generate_single_tool_output(as_no, netid, setup,
                                        make_exit_code == 0)

    # Reference outputs should be all correct
    if netid == reference_dir:
      reference_correct = True
      if setup.exit_code == GradeSetup.SHOULD_BE_ZERO:
        reference_correct = all(elem == 0 for elem in codes.values())
      elif setup.exit_code == GradeSetup.SHOULD_BE_NONZERO:
        reference_correct &= all(elem != 0 for elem in codes.values())
      if not reference_correct:
        sys.stderr.write('Reference\'s output is incorrect: ' +
                         setup.test_dir + '\n')

        # Print incorrect commands
        for cmd in codes.keys():
          if setup.exit_code == GradeSetup.SHOULD_BE_ZERO and \
             codes[cmd] != 0 or \
             setup.exit_code == GradeSetup.SHOULD_BE_NONZERO and \
             codes[cmd] == 0:
            sys.stdout.write('  ' + cmd.cmd + '\n')
        sys.exit(1)
    test_exit_codes.update(codes)

  if make_clean_everytime:
    test_fmk.make_clean(get_student_build_path(as_no))

  return (make_exit_codes, test_exit_codes)

## Output file post-processing functions

# From a file, delete lines that contain the given word
def remove_lines_with_word(filename, word):
  read_f = open(filename, 'r')
  old_lines = read_f.readlines()
  read_f.close()
  new_lines = filter(lambda x:not word in x, old_lines)
  write_f = open(filename, 'w')
  write_f.writelines(new_lines)
  write_f.close()

def transform_file(filename, transform_func):
  read_f = open(filename, 'r')
  old_lines = read_f.readlines()
  read_f.close()
  new_lines = map(transform_func, old_lines)
  write_f = open(filename, 'w')
  write_f.writelines(new_lines)
  write_f.close()

# Remove strings that matche the given regex
def remove_regex(rx_str, str):
  rx = re.compile(rx_str)
  return rx.sub('', str)

# Replace strings that matche the given regex with another string
def replace_regex(rx_str, repl, str):
  rx = re.compile(rx_str)
  return rx.sub(repl, str)

# Only leave the first string strings that matches the given regex and delete
# others
def only_regex(rx_str, str):
  rx = re.compile(rx_str)
  match_list = rx.findall(str)
  if len(match_list) == 0:
    return '\n'
  else:
    return match_list[0] + '\n'

# Remove strings that match the portion within (..) in the given regex
# ex) when rx = '...(something)...', remove strings matching the (something)
# part
def remove_regex_portion(rx_str, str):
  rx = re.compile(rx_str)
  match_obj_list = rx.finditer(str)
  for match in match_obj_list:
    for match_str in match.groups():
      str = str.replace(match_str, '')
  return str

# ex) a.c:12:30: blah blah -> a.c::blah blah
def remove_linecol_in_file(filename):
  func = functools.partial(remove_regex, '\d+:\d+')
  return transform_file(filename, func)

# ex) a.c:12:30: blah blah -> 12:30
def only_linecol_in_file(filename):
  func = functools.partial(only_regex, '\d+:\d+')
  return transform_file(filename, func)

# ex) a.c:12:30: blah blah -> 12
def only_line_in_file(filename):
  func = functools.partial(only_regex, '\d+:')
  return transform_file(filename, func)

# ex) a.c:12:30: blah blah -> 30
def only_col_in_file(filename):
  func = functools.partial(only_regex, ':\d+')
  return transform_file(filename, func)

# ex) 12:30: blah blah: CHAR -> CHAR
def remove_linecol_msg_in_file(filename):
  func = functools.partial(remove_regex_portion, '(\d+:\d+:.*:\s*)[^:]+')
  return transform_file(filename, func)

## Report writing functions

def write_unified_diff(file1, file2, diff_file):
  f1 = open(file1, 'r')
  f2 = open(file2, 'r')
  d = difflib.unified_diff(f1.readlines(), f2.readlines())
  f1.close()
  f2.close()
  diff_f = open(diff_file, 'w')
  diff_f.writelines(d)
  diff_f.close()

def write_summary(report_f, as_no, netid, compile_pass, num_tests, latedays):
  report_f.write('AS%d Assessment Summary\n\n' % as_no)
  report_f.write('NetID:        ' + netid + '\n\n')
  report_f.write('Score:        [_SCORE_]\n') # will be rewritten later
  report_f.write('Compilation:  ')
  if compile_pass:
    report_f.write('PASSED\n')
  else:
    report_f.write('FAILED\n')
  # At the  we write a summary, we don't know the number of passed tests
  # because we get to know it after we compare out/err files
  report_f.write('Correctness:  [_NUM_PASSES_]/%d tests passed\n' % num_tests)
  report_f.write('Late days:    %d\n\n' % latedays)

def write_summary_num_passes(report_file, num_passes):
  func = functools.partial(replace_regex, '\[_NUM_PASSES_\]', str(num_passes))
  return transform_file(report_file, func)

def write_summary_score(report_file, score):
  points_str = '%d/%d' % (round(score), max_score)
  func = functools.partial(replace_regex, '\[_SCORE_\]', points_str)
  return transform_file(report_file, func)

def write_submitted_files(report_f, as_no, netid):
  report_f.write('Assessment Details\n\n')
  report_f.write('files submitted\n')
  report_f.write('-' * 34 + '\n')
  ls_out_f = open(get_ls_out_file(as_no, netid), 'r')
  report_f.write(ls_out_f.read())
  ls_out_f.close()
  report_f.write('\n\n')

def write_compilation_result(report_f, as_no, netid):
  report_f.write('*' * 78 + '\n')
  report_f.write('*  compiling\n')
  report_f.write('*' * 78 + '\n\n\n')
  # Include compile error messages in the report
  report_f.write('$ ' + make_cmd_str + '\n')
  report_f.write('*' + '-' * 63 + '\n')
  make_out_f = open(get_make_out_file(as_no, netid, 'err'), 'r')
  report_f.write(make_out_f.read())
  make_out_f.close()
  report_f.write('=' * 64 + '\n\n\n')

def write_execution_header(report_f):
  report_f.write('*' * 78 + '\n')
  report_f.write('*  executing\n')
  report_f.write('*' * 78 + '\n\n\n')

# Write results for test cases
def write_test_results(report_f, as_no, netid, test_exit_codes):
  # Here we regenerate test commands, but omit all out/err stream handling,
  # because we are not going to run these tests; we already ran those tests
  # when we generated the outputs, and here we only want to check those results.
  # These commands are to be used as keys to 'exit_codes' map.
  # (And __eq__ and __hash__ are overrided for test_fmk.TestCommand, so as long as
  # command string themselves are the same, they will be treated as the same
  # commands.
  num_passes = 0
  total_points = 0.0
  points = 0.0
  test_no = 1

  grade_setups = eval('grade_setup.get_grade_setups_as' + str(as_no))()
  for setup in grade_setups:
    total_points += setup.points
    cur_test_points = 0.0
    dir_desc = get_test_dir_description(get_as_tests_path(as_no) +
                                        setup.test_dir)
    report_f.write('Test %d: ' % test_no + dir_desc +
                   ' (%d points)' % setup.points + '\n')
    test_no += 1

    test_files = []
    if setup.batch:
      flist = glob.glob(get_as_tests_path(as_no) + setup.test_dir + '/*')
      test_files = filter(os.path.isdir, flist)
    else:
      for ext in setup.input_exts:
        test_files += util.get_all_files_recurse(get_as_tests_path(as_no) +
                                                 setup.test_dir, ext)
    if len(test_files) > 0:
      test_points = float(setup.points) / len(test_files)
    else:
      test_points = 0
    test_files.sort()

    for test_file in test_files:
      cmd_str = get_cmd_str(as_no, netid, test_file, setup)

      file_desc = get_test_file_description(test_file)
      report_f.write('  * ' + get_test_file_description(test_file) + ': ')

      out_file = get_corresponding_out_file(
                 as_no, netid, test_file, setup.out_ext)
      err_file = get_corresponding_out_file(
                 as_no, netid, test_file, setup.err_ext)
      ref_out_file = get_corresponding_out_file(
                     as_no, reference_dir, test_file, setup.out_ext)
      ref_err_file = get_corresponding_out_file(
                     as_no, reference_dir, test_file, setup.err_ext)
      exit_code = test_exit_codes[test_fmk.TestCommand(cmd_str)]

      passed = True
      if setup.exit_code == GradeSetup.SHOULD_BE_ZERO and exit_code != 0 or \
         setup.exit_code == GradeSetup.SHOULD_BE_NONZERO and exit_code == 0:
        passed = False

      if not filecmp.cmp(out_file, ref_out_file):
        passed = False
        write_unified_diff(out_file, ref_out_file, out_file + '.diff')
      if not filecmp.cmp(err_file, ref_err_file):
        passed = False
        write_unified_diff(err_file, ref_err_file, err_file + '.diff')

      if passed:
        report_f.write('passed\n')
        num_passes += 1
        points += test_points
        cur_test_points += test_points
      else:
        report_f.write('FAILED\n')
    report_f.write(' Score: %.1f/%.1f\n' %
                   (cur_test_points, setup.points))
    report_f.write('\n')
  return (num_passes, total_points, points)

# Write other points. Refer to misc_points.py
def write_misc_points(report_f, as_no, netid):
  misc_point_list = eval('misc_points.as' + str(as_no))
  total_other_score = 0
  check_no = 1

  for point_map, desc, max_score in misc_point_list:
    report_f.write('Check %d: ' % check_no + desc + '\n')
    check_no += 1

    if not point_map.has_key(netid):
      report_f.write(' Score: %d/%d points\n\n' % (max_score, max_score))
      total_other_score += max_score

    else:
      # Map is either of these two forms:
      # NetID: score
      # NetID: (score, description)
      if type(point_map[netid]) == tuple:
        score, score_desc = point_map[netid]
      else:
        score = point_map[netid]
        score_desc = ''

      if score > max_score:
        sys.stderr.write('misc_points.py: ' + desc + ': ' + netid + ': ')
        sys.stderr.write('Score is bigger than the allowed max score\n')
        sys.exit(1)
      report_f.write(' Score: %d/%d points' % (score, max_score))
      if score_desc != '':
        report_f.write(' (' + score_desc + ')')
      report_f.write('\n\n')
      total_other_score += score
  return total_other_score

# Write other deductions. Refer to misc_deductions.py
def write_misc_deductions(report_f, as_no, netid):
  misc_deduction_map = eval('misc_deductions.as' + str(as_no))
  total_deductions = 0

  if not misc_deduction_map.has_key(netid):
    return 0

  report_f.write('Others:\n')
  for deduction, desc in misc_deduction_map[netid]:
    report_f.write(desc + ': %d' % deduction + '\n')
    total_deductions += deduction

  return total_deductions

# Main grading function for each student
def grade(as_no, netid):
  report_path = get_report_path(as_no, netid)
  (make_exit_codes, test_exit_codes) = generate_output(as_no, netid)
  report_f = open(report_path, 'w')

  make_exit_code = make_exit_codes.values()[0]
  num_tests = len(test_exit_codes.values())

  latedays_map = eval('latedays.as' + str(as_no))
  if netid in latedays_map.keys():
    latedays = latedays_map[netid]
  else:
    latedays = 0
  write_summary(report_f, as_no, netid, make_exit_code==0, num_tests, latedays)

  write_submitted_files(report_f, as_no, netid)
  write_compilation_result(report_f, as_no, netid)
  write_execution_header(report_f)
  (num_passes, total_score, score) = \
    write_test_results(report_f, as_no, netid, test_exit_codes)

  score += write_misc_points(report_f, as_no, netid)
  score += write_misc_deductions(report_f, as_no, netid)
  if score < 0:
    score = 0
  elif score > 100:
    score = 100

  report_f.close()
  write_summary_num_passes(report_path, num_passes)
  write_summary_score(report_path, score)
  return round(score)

if __name__ == '__main__':
  # sys.argv[1] = assignment number
  # sys.argv[2]~ = optional netids - if specified, grades only those students
  if len(sys.argv) < 2 or not sys.argv[1].isdigit():
    sys.stderr.write('Usage: python ' + sys.argv[0] +
                     ' ASSIGNMENT_NUMBER [NetIDs..]\n')
    sys.exit(1)

  as_no_str = sys.argv[1]
  as_no = int(as_no_str)
  only_netids = sys.argv[2:]

  # Reference output generation
  generate_output(as_no, reference_dir)

  # Student output generation & grading
  scores = []
  for netid in sorted(os.listdir(get_as_submit_path(as_no))):
    if netid == reference_dir or netid == '.svn':
      continue
    if len(only_netids) > 0 and not netid in only_netids:
      continue
    print netid
    scores.append(grade(as_no, netid))

  if len(scores) > 0:
    print
    print 'Mean = %1.f' % numpy.mean(scores)
    print 'Median = %1.f' % numpy.median(scores)
    print 'Min = %d' % min(scores)
    print 'Max = %d' % max(scores)
