// This is a part of code I wrote as a TA for COS320 "Compiling Techniques"
// course in Spring 2015. It was the reference solution of one of the
// assignments.
// This is an LLVM pass that performs Superblock formation.

#define DEBUG_TYPE "superblock-formation"

#include "SuperblockFormation.h"
#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Support/Debug.h"

using namespace llvm;
using namespace fun;

STATISTIC(numClonedBBs, "The # of cloned basic blocks");

char SuperblockFormation::ID = 0;
static RegisterPass<SuperblockFormation>
rp("superblock-formation", "Superblock Formation", false, false);

static cl::opt<double>
biasThreshold("superblock-branch-bias-threshold", cl::init(0.7),
             cl::NotHidden, cl::Optional, cl::ValueRequired,
             cl::value_desc("threshold"),
             cl::desc("Branch bias threshold to trigger superblock formation"));
/*
static cl::opt<unsigned>
maxCodeExpansionFactor("max-code-expansion-factor", cl::init(2),
                       cl::NotHidden, cl::Optional, cl::ValueRequired,
                       cl::value_desc("num_insts"),
                       cl::desc("Maximum ratio of code expansion "
                                " (used by -superblock-formation)"));
*/

// Don't impose any limit for now
static const int maxCodeExpansionFactor = 10000;

void SBTrace::print(raw_ostream &out) const {
  out << "SBTrace: \n";
  out << "BBs: ";
  for (auto &bb : bbs())
    out << bb->getName() << " ";
  out << "\nDuplicated BBs: ";
  for (auto &bb : dupBBs())
    out << bb->getName() << " ";
  out << "\n";
}

void SBTrace::dump() const {
  print(dbgs());
}

bool SuperblockFormation::runOnModule(Module &m) {
  bbLoader = &getAnalysis<BBProfileLoader>();
  edgeLoader = &getAnalysis<EdgeProfileLoader>();

  // Compute original code size
  unsigned origCodeSize = 0;
  for (Module::iterator f = m.begin(), e = m.end(); f != e; ++f) {
    if (f->isDeclaration()) continue;
    for (Function::iterator bb = f->begin(), e = f->end(); bb != e; ++bb)
      origCodeSize += getNumNonPHIInsts(bb);
  }
  unsigned maxCodeSize = maxCodeExpansionFactor * origCodeSize;

  // Superblock formation
  for (Module::iterator f = m.begin(), e = m.end(); f != e; ++f) {
    if (f->isDeclaration()) continue;

    DEBUG(dbgs() << "\nFunction: " << f->getName() << "\n");
    LoopInfo &li = getAnalysis<LoopInfo>(*f);

    // Detect innermost loops
    LoopVecTy innermostLoops;
    for (LoopInfo::iterator i = li.begin(), e = li.end(); i != e; ++i) {
      Loop *l = *i;
      getInnermostLoops(l, innermostLoops);
    }

    // Collect traces
    LoopToTracesMapTy loop2Traces;
    for (auto &l : innermostLoops) {
      DEBUG(dbgs() << "Innermost loop:\n");
      DEBUG(l->dump());
      collectTrace(l, loop2Traces);
    }

    // Duplicate tails
    unsigned curCodeSize = 0;
    for (auto &kv : loop2Traces) {
      auto &traces = kv.second;
      for (auto &trace : traces)
        duplicateTail(trace, maxCodeSize, curCodeSize);
    }

    for (auto &kv : loop2Traces) {
      auto &traces = kv.second;
      for (auto &trace : traces)
        delete trace;
    }
  }

  if (verifyModule(m, &dbgs()))
    assert(false);

  return true;
}

void SuperblockFormation::getInnermostLoops(Loop *l, LoopVecTy &innermostLoops)
  const {
  if (l->getSubLoops().empty()) {
    innermostLoops.push_back(l);
    DEBUG(dbgs() << "Innermost loop found:\n");
    DEBUG(l->dump());
    return;
  }

  for (Loop::iterator sl = l->begin(), e = l->end(); sl != e; ++sl)
    getInnermostLoops(*sl, innermostLoops);
}

void SuperblockFormation::collectTrace(Loop *l, LoopToTracesMapTy &loop2Traces)
{
  BBSetTy visitedBBs;

  // We pick a best BB and grow a trace forward and backward from it until
  // the next best successor/predecessor satisfies the bias threshold.
  // After collecting a trace, we collect another trace in the same way among
  // remaining BBs. We do this until there is no more trace to be found.
  // We ignore one-block traces.
  while (true) {
    // There is only one BB left
    if (l->getNumBlocks() == visitedBBs.size() + 1)
      break;

    BasicBlock *bestBB = getBestBB(l, visitedBBs);
    if (!bestBB) break;

    visitedBBs.insert(bestBB);
    SBTrace *trace = new SBTrace(bestBB, l);
    growTraceForward(trace, visitedBBs);
    growTraceBackward(trace, visitedBBs);
    if (trace->size() > 1)
      loop2Traces[l].push_back(trace);
  }
}

void SuperblockFormation::growTraceForward(
    SBTrace *trace, BBSetTy &visitedBBs) {
  DEBUG(dbgs() << "growTraceForward:\n");
  BasicBlock *curBB = trace->back();
  while (true) {
    BasicBlock *succ = getBestSucc(curBB, trace->getLoop(), visitedBBs);
    if (!succ) break;
    EdgeProfileLoader::Edge edge(curBB, succ);
    if (edgeLoader->getWeight(edge) <= biasThreshold) {
      DEBUG(dbgs() << " Less than threshould. break\n");
      break;
    }
    trace->pushBack(succ);
    visitedBBs.insert(succ);
    curBB = succ;
  }
}

void SuperblockFormation::growTraceBackward(
    SBTrace *trace, BBSetTy &visitedBBs) {
  DEBUG(dbgs() << "growTraceBackward:\n");
  BasicBlock *curBB = trace->front();
  while (true) {
    BasicBlock *pred = getBestPred(curBB, trace->getLoop(), visitedBBs);
    if (!pred) break;
    EdgeProfileLoader::Edge edge(pred, curBB);
    if (edgeLoader->getWeight(edge) < biasThreshold) {
      DEBUG(dbgs() << " Less than threshould. break\n");
      break;
    }
    trace->pushFront(pred);
    visitedBBs.insert(pred);
    curBB = pred;
  }
}

// Get unvisited BB with highest execution count
BasicBlock *SuperblockFormation::getBestBB(
    const Loop *l, const BBSetTy &visitedBBs) const {
  unsigned bestCount = 0;
  BasicBlock *bestBB = nullptr;
  for (Loop::block_iterator bi = l->block_begin(), be = l->block_end();
       bi != be; ++bi) {
    BasicBlock *bb = *bi;
    if (visitedBBs.count(bb)) continue;
    DEBUG(dbgs() << "Checking BB: " << bb->getName() << "\n");
    if (bbLoader->getCount(bb) > bestCount) {
      bestCount = bbLoader->getCount(bb);
      bestBB = bb;
    }
  }

  if (bestBB)
    DEBUG(dbgs() << "Best BB: " << bestBB->getName() << ": " << bestCount
                 << "\n");
  else
    DEBUG(dbgs() << "Best BB not found\n");
  return bestBB;
}

BasicBlock *SuperblockFormation::getBestPred(
    BasicBlock *bb, const Loop *l, const BBSetTy &visitedBBs) const {
  // We don't want the predecessor to be a latch
  if (l->getHeader() == bb) {
    DEBUG(dbgs() << "This is header: no pred\n");
    return nullptr;
  }

  BasicBlock *bestPred = nullptr;
  unsigned bestCount = 0;
  double bestWeight = 0;

  // We keep track of this because there can be two edges from the same block
  BBSetTy visitedPreds;

  for (pred_iterator pi = pred_begin(bb), e = pred_end(bb); pi != e; ++pi) {
    BasicBlock *pred = *pi;
    if (visitedPreds.count(pred)) continue;
    visitedPreds.insert(pred);

    EdgeProfileLoader::Edge edge(pred, bb);
    if (edgeLoader->getWeight(edge) > bestWeight) {
      bestCount = edgeLoader->getCount(edge);
      bestWeight = edgeLoader->getWeight(edge);
      bestPred = pred;
    }
  }

  if (bestPred && !visitedBBs.count(bestPred) && l->contains(bestPred)) {
    DEBUG(dbgs() << "Best predecessor: " << bestPred->getName() << ": "
                 << bestCount << "(" << bestWeight << ")\n");
    return bestPred;
  }
  else {
    DEBUG(dbgs() << "Best predecessor not found\n");
    return NULL;
  }
}

BasicBlock *SuperblockFormation::getBestSucc(
    BasicBlock *bb, const Loop *l, const BBSetTy &visitedBBs) const {
  BasicBlock *bestSucc = nullptr;
  unsigned bestCount = 0;
  double bestWeight = 0;

  // We keep track of this because two edges can go to the same block
  BBSetTy visitedSuccs;
  for (succ_iterator si = succ_begin(bb), e = succ_end(bb); si != e; ++si) {
    BasicBlock *succ = *si;
    if (visitedSuccs.count(succ)) continue;
    visitedSuccs.insert(succ);

    EdgeProfileLoader::Edge edge(bb, succ);
    if (edgeLoader->getWeight(edge) > bestWeight) {
      bestCount = edgeLoader->getCount(edge);
      bestWeight = edgeLoader->getWeight(edge);
      bestSucc = succ;
    }
  }

  if (bestSucc && !visitedBBs.count(bestSucc) && l->contains(bestSucc) &&
      l->getHeader() != bestSucc) {
    DEBUG(dbgs() << "Best successor: " << bestSucc->getName() << ": "
                 << bestCount << "(" << bestWeight << ")\n");
    return bestSucc;
  }
  else {
    DEBUG(dbgs() << "Best successor not found\n");
    return NULL;
  }
}

unsigned SuperblockFormation::getNumNonPHIInsts(const BasicBlock *bb) const {
  unsigned numNonPHIInsts = 0;
  for (BasicBlock::const_iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
    if (!dyn_cast<PHINode>(i))
      numNonPHIInsts++;
  }
  return numNonPHIInsts;
}

void SuperblockFormation::duplicateTail(
    SBTrace *trace, unsigned maxCodeSize, unsigned &curCodeSize) {
  DEBUG(dbgs() << "\nduplicateTail:\n");
  DEBUG(trace->dump());
  figureOutBlocksToDuplicate(trace, maxCodeSize, curCodeSize);

  // In this case, we don't need to duplicate any blocks because there is
  // no side entrace
  if (!trace->hasDupBlocks()) {
    DEBUG(dbgs() << "This trace has no dup blocks\n");
    return;
  }

  ValueToValueMapTy vmap;
  cloneBBs(trace, vmap);
  fixSideEntrances(trace, vmap);
  fixPHINodes(trace, vmap);
  updateSSA(trace, vmap);
}

// Returns true if the given block has a side entrance outside the trace.
// Always returns false for the first block of a trace.
bool SuperblockFormation::hasSideEntrance(
    const BasicBlock *bb, SBTrace *trace) const {
  if (pred_begin(bb) == pred_end(bb)) return false; // no predecesor
  if (trace->isFront(bb)) return false; // first bb in trace

  for (const_pred_iterator pi = pred_begin(bb), e = pred_end(bb); pi != e; ++pi)
  {
    if (trace->prev(bb) != *pi)
      return true;
  }
  return false;
}

void SuperblockFormation::figureOutBlocksToDuplicate(
    SBTrace *trace, unsigned maxCodeSize, unsigned &curCodeSize) const {
  for (auto &bb : trace->bbs()) {
    if (trace->getDupStartBB() &&
        curCodeSize + getNumNonPHIInsts(bb) > maxCodeSize) {
      DEBUG(dbgs() << "Code size limit reached: BB = " << bb->getName()
                   << "  maxCodeSize = " << maxCodeSize << "  curCodeSize = "
                   << curCodeSize << "  curBB = " << getNumNonPHIInsts(bb)
                   << "\n");
      trace->setDupEndBB(bb); // exclusive
      break;
    }
    if (hasSideEntrance(bb, trace) && !trace->getDupStartBB()) {
      trace->setDupStartBB(bb); // inclusive
      DEBUG(dbgs() << "Dup start BB = " << bb->getName() << "\n");
    }
    if (trace->getDupStartBB())
      curCodeSize += getNumNonPHIInsts(bb);
  }
}

void SuperblockFormation::cloneBBs(SBTrace *trace, ValueToValueMapTy &vmap) {
  // Clone necessary BBs
  for (auto &bb : trace->dupBBs()) {
    BasicBlock *cloneBB = CloneBasicBlock(bb, vmap, ".clone");
    bb->getParent()->getBasicBlockList().push_back(cloneBB);
    vmap[bb] =  cloneBB; // add BBs too
    DEBUG(dbgs() << "Cloned: " << bb->getName() << " -> " << cloneBB->getName()
                 << "\n");
    ++numClonedBBs;
  }

  // Change value mappings
  // ex)
  // - original code:
  //   %a = 1 + %k
  //   %b = 1 + %a
  // When cloned, only destinations are cloned. So this becomes:
  // - cloned code:
  //   %a.clone = 1 + %k
  //   %b.clone = 1 + %a
  // So, we need to remap values:
  // - remapped code:
  //   %a.clone = 1 + %k
  //   %b.clone = 1 + %a.clone
  for (auto &bb : trace->dupBBs()) {
    BasicBlock *cloneBB = cast<BasicBlock>(vmap[bb]);
    for (BasicBlock::iterator i = cloneBB->begin(), e = cloneBB->end(); i != e;
         ++i)
      remapInstruction(i, vmap);
  }
}

// From lib/Transforms/Utils/LoopUnroll.cpp
// (It is a static function so I can't call it)
void SuperblockFormation::remapInstruction(
    Instruction *i, ValueToValueMapTy &vmap) {
  for (unsigned idx = 0, e = i->getNumOperands(); idx != e; ++idx) {
    Value *op = i->getOperand(idx);
    ValueToValueMapTy::iterator it = vmap.find(op);
    if (it != vmap.end())
      i->setOperand(idx, it->second);
  }

  // It looks like only incoming values are included in the operand.
  // We need to handle incoming blocks separately..
  if (PHINode *pn = dyn_cast<PHINode>(i)) {
    for (unsigned j = 0, e = pn->getNumIncomingValues(); j != e; ++j) {
      ValueToValueMapTy::iterator it = vmap.find(pn->getIncomingBlock(j));
      if (it != vmap.end())
        pn->setIncomingBlock(j, cast<BasicBlock>(it->second));
    }
  }
}

void SuperblockFormation::fixSideEntrances(
    SBTrace *trace, const ValueToValueMapTy &vmap) {
  // Adjust side entrances
  ValueToValueMapTy bbMap; // we can't use vmap because it has also other values
  for (auto &bb : trace->dupBBs()) {
    if (!hasSideEntrance(bb, trace)) continue;

    assert(vmap.count(bb) && "BB is not in the vmap");
    Value *v = vmap.lookup(bb);
    BasicBlock *cloneBB = cast<BasicBlock>(v);
    BasicBlock *tracePred = trace->prev(bb); // predecessor in the trace

    bbMap.clear();
    bbMap[bb] = cloneBB;
    for (pred_iterator pi = pred_begin(bb), e = pred_end(bb); pi != e;) {
      BasicBlock *pred = *pi;
      ++pi; // remapInstruction() will invalidate this iterator, so..
      if (pred == tracePred) continue;
      remapInstruction(pred->getTerminator(), bbMap);
    }
  }
}

void SuperblockFormation::fixPHINodes(
    SBTrace *trace, const ValueToValueMapTy &vmap) {

  // Fix phi instructions - original BBs
  // For original BBs, we need to remove phis because side entrances will go
  // to copied BBs. For each phinodes, Replace all uses of the phi with the
  // value from its predecessor in the trace
  DEBUG(dbgs() << "fixPHINodes: original BBs\n");
  for (auto &bb : trace->dupBBs()) {
    BasicBlock *tracePred = trace->prev(bb); // predecessor in the trace
    BasicBlock::iterator i = bb->begin();
    while (PHINode *pn = dyn_cast<PHINode>(i++)) {
      DEBUG(dbgs() << "phi node = " << pn->getName() << "\n");
      for (unsigned j = 0;  j != pn->getNumIncomingValues(); ++j) {
        DEBUG(dbgs() << "  incoming block = "
                     << pn->getIncomingBlock(j)->getName() << "\n");
        if (pn->getIncomingBlock(j) == tracePred) continue;
        // Delete all other incoming values except the one from the predecessor
        // within the trace.
        pn->removeIncomingValue(j);
        j--;
        DEBUG(dbgs() << "  removed\n");
      }
    }
  }

  // Fix phi instructions - clone BB
  // For each phinode in the first clone BB, we need to delete a predecessor in
  // the original trace
  DEBUG(dbgs() << "fixPHINodes: first clone BB\n");
  BasicBlock *firstDupBB = *trace->dupBB_begin();
  BasicBlock *tracePredFirstDupBB = trace->prev(firstDupBB);
  Value *v = vmap.lookup(firstDupBB);
  BasicBlock *firstCloneBB = cast<BasicBlock>(v);
  BasicBlock::iterator i = firstCloneBB->begin();

  while (PHINode *pn = dyn_cast<PHINode>(i++)) {
    for (unsigned j = 0;  j != pn->getNumIncomingValues(); ++j) {
      DEBUG(dbgs() << "  incoming block = "
                   << pn->getIncomingBlock(j)->getName() << "\n");
      if (pn->getIncomingBlock(j) != tracePredFirstDupBB) continue;
      // Delete the incoming value from the predecessor in the trace, because
      // that predecessor will now jump to not this node but the successor
      // within the trace.
      pn->removeIncomingValue(j);
      DEBUG(dbgs() << "  removed\n");
      break;
    }
  }

  // We need to insert a new incoming block/value pair in phi nodes at the point
  // a block and its corresponding cloned block have a common successor.
  // ex)
  //   bb1
  //    |
  //   bb2     bb2.clone
  //    |         |
  //   bb3--------|
  // bb2 is cloned to bb2.clone and bb3 was originally bb2's successor, now bb3
  // has additional predecessor bb2.clone. If bb3 had one or more phi nodes in
  // it, we should add bb2.clone to their incoming block.
  for (auto &bb : trace->dupBBs()) {
    if (!vmap.count(bb)) continue;
    Value *v = vmap.lookup(bb);
    BasicBlock *cloneBB = cast<BasicBlock>(v);
    BasicBlock *traceSucc = trace->next(bb); // successor in the trace

    BBSetTy visitedSuccs;
    TerminatorInst *ti = bb->getTerminator();
    for (unsigned s = 0, e = ti->getNumSuccessors(); s != e; ++s) {
      BasicBlock *succ = ti->getSuccessor(s);
      if (visitedSuccs.count(succ)) continue;
      if (succ == traceSucc) continue;

      BasicBlock::iterator i = succ->begin();
      while (PHINode *pn = dyn_cast<PHINode>(i++)) {
        int idx = pn->getBasicBlockIndex(bb);
        Value *v = pn->getIncomingValue(idx);
        Value *cloneV = vmap.count(v) ? (Value*) vmap.lookup(v) : v;
        pn->addIncoming(cloneV, cloneBB);
      }
    }
  }
}

void SuperblockFormation::updateSSA(SBTrace *trace, ValueToValueMapTy &vmap) {
  SSAUpdater ssa;

  for (const auto &kv : vmap) {
    Value *origV = const_cast<Value*>(kv.first);
    Value *copyV = kv.second;
    Instruction *orig = dyn_cast<Instruction>(origV);
    Instruction *copy = dyn_cast<Instruction>(copyV);
    if (!orig) continue; // this is not an instruction but a BB

    ssa.Initialize(orig->getType(), orig->getName());

    DEBUG(dbgs() << "SSA Initialize: " << orig->getName() << "\n");
    ssa.AddAvailableValue(orig->getParent(), orig);
    DEBUG(dbgs() << "  AddAvailableValue:\n");
    DEBUG(dbgs() << "  Basicblock = " << orig->getParent()->getName() << "\n");
    DEBUG(orig->dump());
    ssa.AddAvailableValue(copy->getParent(), copy);
    DEBUG(dbgs() << "  AddAvailableValue:\n");
    DEBUG(dbgs() << "  Basicblock = " << copy->getParent()->getName() << "\n");
    DEBUG(copy->dump());

    for (Value::use_iterator ui = orig->use_begin(), e = orig->use_end();
         ui != e;) {
      Use &u = *ui;
      ++ui; // ReWriteUse will invalidate this iterator, so..
      rewriteUse(ssa, u);
    }
    for (Value::use_iterator ui = copy->use_begin(), e = copy->use_end();
         ui != e;) {
      Use &u = *ui;
      ++ui; // ReWriteUse will invalidate this iterator, so..
      rewriteUse(ssa, u);
    }
  }
}

// This is a function to handle SSAUpdter's (in my opinion) kinks.
// When the definition and the use are in different blocks, SSAUpdater's
// RewriteUse() and RewriteUseAfterInsertions() are basically the same.
// But when they are in the same block, things get little tricky.
// RewriteUse does not handle in-block definitions; any 'AddAvailableValue's
// added for the use's block will be considered to be below it.
//
// ex) When %a is added as an available value and a BB is like this:
// %a = %b + 1  --- (1)
// %d = %a + 2  --- (2)
//
// Even though the definition of %a is above (2) so the use of %a in (2) should
// use the defintion in (1), RewriteUse() assumes the available value of %a in
// (1) to be below (2). On the other hand, RewriteUseAfterInsertions() always
// assumes the def to be above the use if they are in the same block. So it can
// handle the situation above correctly, but does not handle this situation:
//
// ex) When %a is added as an available value and (1) and (2) are swapped from
// the situation above:
// %d = %a + 2  --- (1)
// %a = %b + 1  --- (2)
//
// Here the use of %a in (1) should use definitions from one of its
// predecessors. But RewriteUseAfterInsertions() incorrectly thinks the use in
// (1) is using the def in (2).
// So what I basically do in this function is, if the current BB has an
// available def, call the right function based on the position of the def and
// use instructions.
void SuperblockFormation::rewriteUse(SSAUpdater &ssa, Use &u) {
  Instruction *useI = cast<Instruction>(u.getUser());
  DEBUG(dbgs() << "rewriteUse: useI = ");
  DEBUG(useI->dump());
  BasicBlock *bb = useI->getParent();

  if (ssa.HasValueForBlock(bb)) {
    DEBUG(dbgs() << " HasValueForBlock = " << bb->getName() << "\n");
    Instruction *defI = cast<Instruction>(ssa.GetValueAtEndOfBlock(bb));
    DEBUG(dbgs() << " defI = ");
    DEBUG(defI->dump());
    DEBUG(bb->dump());

    // SSAUpdater.HasValueForBlock() returns true even for some BBs that have
    // not been inserted by AddAvailableValue(), because RewriteUse() and
    // RewriteUseAfterInsertions() internally inserts a default value for
    // basic blocks they are quering. So we double check if the def is in the
    // same BB with the use here.
    if (defI->getParent() == bb) {
      if (isBeforeInBB(bb, defI, useI))
        ssa.RewriteUseAfterInsertions(u);
      else
        ssa.RewriteUse(u);

    } else
      ssa.RewriteUseAfterInsertions(u); // RewriteUse() is also ok

  } else
    ssa.RewriteUseAfterInsertions(u); // RewriteUse() is also ok
}

bool SuperblockFormation::isBeforeInBB(
    const BasicBlock *bb, const Instruction *i1, const Instruction *i2) const {
  assert(i1->getParent() == bb && "Inst1 is not in the BB");
  assert(i2->getParent() == bb && "Inst2 is not in the BB");
  bool hasSeenI1 = false;
  for (BasicBlock::const_iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
    const Instruction *curI = i;
    if (curI == i1)
      hasSeenI1 = true;
    if (curI == i2)
      return hasSeenI1;
  }
  assert(false);
}
